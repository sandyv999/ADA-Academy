# !/usr/bin/python
# Run this script if you want to check if a server is online. It takes input on the command line as an IP or name.  
# Works under Windows and Linux using Python 3
# Sandy Vasquez: Initial version April 17, 2017
# Library Imports

import socket
import subprocess as sp
import csv
import re
import sys  
import os
import platform

sys.argv = [sys.argv[0], 'bellevuecollege.edu']

name= sys.argv[1]

    
#Ping for Linux
def pingForUnix(ip):
    status, result = sp.getstatusoutput("ping -c1 -w2 " + str(ip))
    if status == 0:
        time = result.split("\n")[1]
        time = re.findall(".+time=([0-9.]+) ms", time)[0]
        return time
    else:
        return "null"

#Ping for Windows
def pingForWindows(ip):
    status, result = sp.getstatusoutput("ping -n 1 " + str(ip))
    if status == 0:
        time = result.split("\n")[2]
        time = re.findall(".+time[=<]([0-9.]+)ms.+", time)[0]
        return time
    else:
        return "null"
    
    
#Loop through the input and ping each item
print("IP, TimeToPing (ms)")
ip = socket.gethostbyname(name)
    
if platform.system()=="Windows":
        time_to_ping = pingForWindows(ip)
else:
        time_to_ping = pingForUnix(ip)

if time_to_ping=="null":
        print("Not Found", "null", sep= " , ")
else:
        print(ip, time_to_ping, sep=" , ")

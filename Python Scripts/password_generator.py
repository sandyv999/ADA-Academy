#This script creates a random password that starts with 'a'
##and then has 11 digits picked randomly after it
#Sandy Vasquez: Initial version, May 05, 2017 
#Library Imports

import random

#for the password

alphabet = "a"

pw_length = 11
mypw = ""

for i in range(pw_length):
    next_index = random.randrange(len(alphabet))
    mypw = mypw + alphabet[next_index]


for i in range(random.randrange(1,3)):
    replace_index = random.randrange(len(mypw)//2)
    mypw = mypw[0:replace_index] + str(random.randrange(10)) + mypw[replace_index+1:]


print(mypw)

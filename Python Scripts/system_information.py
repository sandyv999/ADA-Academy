# !/usr/bin/python
# Run this script if you want to check your system information
# Works under Windows and Linux
# Sandy Vasquez: Initial version April 17, 2017
# Library Imports

import re
import subprocess as sp
import multiprocessing
import psutil
import os
import platform
import socket

print("%s | %s | %s | %s | %s | %s"%("CPU(count)", "RAM(GB)", "OSType", "OSVersion", "Disk(count)", "IP"))
if platform.system()=="Windows":
    #Windows
    cpu_count = multiprocessing.cpu_count()
    mem = round(psutil.virtual_memory().total/(1024*1024*1024), 2)
    os = platform.system()
    version = platform.version()

    #partitions
    partitionCount = 0
    for disk in psutil.disk_partitions():
        if disk.opts=="rw,fixed":
            partitionCount += 1

    ip = socket.gethostbyname(socket.gethostname())
else:
    #Linux
    cpu_count = multiprocessing.cpu_count()
    mem = round(psutil.virtual_memory().total/(1024*1024*1024), 2)
    os = platform.system()
    version = platform.linux_distribution()[1]

    partitionCount = len(psutil.disk_partitions())

    ip = socket.gethostbyname(socket.gethostname())

print("%s | %s | %s | %s | %s | %s"%(cpu_count, mem, os, version, partitionCount, ip))

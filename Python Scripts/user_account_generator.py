# This will take user input and create accounts that is output to a csv. file
# Works under Windows and Linux
# Sandy Vasquez: Initial version, May 5, 2017 
# Library Imports


import csv


contin = True
ListTofile = []
ListTofile.append(['First Name', 'Last Name', 'school', 'accountType', 'Userdid'])

# index_staff keeps index to manage duplicates
index_staff = {}

# index_student keeps index to manage duplicates
index_student = {}

while contin:
    firstName = input('Please enter the first name :\n')
    lastName = input('Please enter the last name :\n')
    school = input('Please enter the school name :\n')
    quit = False
    # Error checking for school input
    while school not in ['IHS', 'NJH', 'AH', 'BHS', 'BC']:
        print('The school name must be one of the followings: ', ['IHS', 'NJH', 'AH', 'BHS', 'BC'])
        school = input('Please enter the school name  or 0 to exit:\n')
        if school == '0':
            quit = True
            break

    accountType = input('Please enter the account type :\n')

    # Error checking for account type
    while accountType.lower() not in ['student', 'staff']:
        print('The account type must be one of the followings: ', ['Student', 'Staff'])
        accountType = input('Please enter the account type 0 to exit:\n')
        if accountType == '0':
            quit = True
            break

    # Userid account generator for staff
    if accountType.lower() == 'staff':
        userid = firstName[0] + lastName
        if userid in index_staff.keys():
            index_staff[userid] += 1
            userid = userid + str(index_staff[userid])

        else:
            index_staff[userid] = 1

    # Userid account generator for student
    # first 5 of last name + first 3 of first name
    # + NN if duplicates are present
    if accountType.lower() == 'student':
        userid = lastName[:5] + firstName[:3]
        if userid in index_student.keys():
            index = int(index_student[userid]) + 1
            # if index < 10 then represent it like NN : 02, 05 etc
            index = '{:02}'.format(index)
            index_student[userid] = index
            userid = userid + index
        else:
            index_student[userid] = '00'
            userid = userid + '00'


    ListTofile.append([firstName, lastName, school, accountType, userid])

    # Looping
    yesOrNo = input('\n Do you wish to continue enter yes or no:\n ')
    if yesOrNo == 'no':
        break

# Naming the csv. file
with open("user_account_generator.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(ListTofile)
